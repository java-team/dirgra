Source: dirgra
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               javahelper,
               junit4,
               libmaven-javadoc-plugin-java,
               libmaven-source-plugin-java,
               maven-debian-helper,
               maven-repo-helper
Standards-Version: 4.6.2
Homepage: https://github.com/jruby/dirgra
Vcs-Git: https://salsa.debian.org/java-team/dirgra.git
Vcs-Browser: https://salsa.debian.org/java-team/dirgra
Rules-Requires-Root: no

Package: libdirgra-java
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${maven:Depends}
Description: Java library providing a simple directed graph implementation
 A directed graph (or digraph) is a graph (that is a set of vertices
 connected by edges), where the edges have a direction associated with
 them.
 .
 This library is currently used in JRuby implementation but it's perfectly
 reusable for any other project requiring this kind of data structures.

Package: libdirgra-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Suggests: libdirgra-java
Description: Documentation for dirgra
 A directed graph (or digraph) is a graph (that is a set of vertices
 connected by edges), where the edges have a direction associated with
 them.
 .
 This library is currently used in JRuby implementation but it's perfectly
 reusable for any other project requiring this kind of data structures.
 .
 This package contains the API documentation of libdirgra-java.
